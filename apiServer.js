var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


// APIs

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/bookshop');

const db = mongoose.connection;
db.on('error', console.error.bind(console, '# MongoDB - connection error'));

// ---->> SET UP SESSIONS <<----
app.use(session({
  secret: 'mySecretString',
  saveUninitialized: false,
  resave: false,
  cookie: {maxAge: 1000 * 60 * 60 * 24 * 2}, // 2 days in milliseconds
  store: new MongoStore({mongooseConnection: db, ttl: 2 * 24 * 60 * 60})
  //ttl: 2 days * 24 hours * 60 minutes * 60 seconds
}));

// SAVE TO SESSION
app.post('/cart', (req, res) => {
  let cart = req.body;
  req.session.cart = cart;
  req.session.save(err => {
    if (err) {
      console.log('server error', err);
    }

    res.json(req.session.cart);
  });
});

//GET SESSION CART API
app.get('/cart', (req, res) => {
  if (typeof req.session.cart !== 'undefined') {
    res.json(req.session.cart);
  }
});

// ---->> END SESSION SET UP <<----

const Books = require('./models/books.js');

// ----- Post Book -----

app.post('/books', (req, res) => {
  let book = req.body;

  Books.create(book, (err, books) => {
    if (err) {
      console.log('server error', err);
    }

    res.json(books);
  });
});

//------>> GET ALL BOOKS <<----------
app.get('/books', (req, res) => {
  Books.find({}, (err, books) => {
    if (err) {
      console.log('server error', err);
    }

    res.json(books);
  });
});

// ------->> DELETE BOOK <<----

app.delete('/books/:_id', (req, res) => {
  const query = {_id: req.params._id };

  Books.remove( query, (err, books) => {
    if (err) {
      console.log('server error', err);
    }
    res.json(books);
  })
});

//---->> UPDATE BOOKS <<-----

app.put('/books/:_id', (req, res) => {
  let book = req.body;
  let query = req.params._id;

  let update = {
    '$set': {
      title: book.title,
      description: book.description,
      image: book.image,
      price: book.price
    }
  };

  let options = { new: true };

  Books.findByIdAndUpdate(query, update, options, (err, books) => {
    if (err) {
      console.log('server error', err);
    }

    res.json(books);
  });

});

// ---->> GET BOOKS IMAGES API <<----

app.get('/images', (req, res) => {
  const imgFolder = path.join(__dirname, 'public', 'images');
  const fs = require('fs');

  fs.readdir(imgFolder, (err, files) => {
    if (err) {
      return console.error(err);
    }

    const imgNames = [];
    files.forEach(file => {
      imgNames.push({ name: file });
    })

    res.json(imgNames);
  });
});

//END APIs

app.listen(3001, err => {
  if (err) {
    return console.log(err);
  }

  console.log('API Server run on port 3001');
})
