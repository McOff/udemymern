import React from 'react';
import { Row, Col, Well, Button, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addToCart, updateCart } from '../../actions/cartActions';

class BookItem extends React.Component{
  constructor() {
    super();
    this.state = {
      isClicked: false
    }
  }

  onReadMore() {
    this.setState({isClicked: true});
  }

  handleCart() {
    const { _id, title, description, price, images } = this.props;
    const book = [ ...this.props.cart, {
      _id,
      title,
      description,
      images,
      price,
      quantity: 1
    }];
    // CHECK IF THE CART IS EMPTY
    if (this.props.cart.length > 0) {
      let bookIndex = this.props.cart.findIndex(book => book._id === _id);

      bookIndex < 0 ? this.props.addToCart(book) : this.props.updateCart(_id, 1, this.props.cart);

    } else {
      this.props.addToCart(book);
    }

  }

  render() {
    return (
      <Well>
        <Row>
          <Col xs={12} sm={4}>
            <Image src={this.props.images} responsive />
          </Col>
          <Col xs={6} sm={8}>
            <h6>{this.props.title}</h6>
            <p>
              {(this.props.description.length > 50 && !this.state.isClicked) ?
                (this.props.description.substring(0, 50)) :
                (this.props.description)}
              <button className='link' onClick={this.onReadMore.bind(this)}>
                {(this.state.isClicked === false && this.props.description &&
                this.props.description.length > 50) ? ('...read more') : ('')}
              </button>
            </p>
            <h6>uds. {this.props.price}</h6>
            <Button onClick={this.handleCart.bind(this)} bsStyle='primary'>Buy now</Button>
          </Col>
        </Row>
      </Well>
    )
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart.cart
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addToCart, updateCart
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookItem);
