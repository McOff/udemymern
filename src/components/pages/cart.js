import React from 'react';
import { connect } from 'react-redux';
import { Modal, Panel, Col, Row, Well, Button, ButtonGroup, Label } from 'react-bootstrap';
import { deleteCartItem, updateCart, getCart } from '../../actions/cartActions';

class Cart extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false
    }
  }

  componentDidMount() {
    this.props.getCart();
  }

  open() {
    this.setState({
      showModal: true
    })
  }

  close() {
    this.setState({
      showModal: false
    })
  }

  onDelete(_id) {
    let booksToDelete = [...this.props.cart];
    let idxToDel = booksToDelete.findIndex( book => book._id === _id);
    let cartAfterDelete = [...booksToDelete.slice(0, idxToDel), ...booksToDelete.slice(idxToDel + 1)];

    this.props.deleteCartItem(cartAfterDelete);
  }

  onIncrement(_id) {
    this.props.updateCart(_id, 1, this.props.cart);
  }

  onDecrement(_id, qnt) {
    if (qnt > 1) {
      this.props.updateCart(_id, -1, this.props.cart);
    }
  }

  render() {
    if(this.props.cart[0]) {
      return this.renderCart();
    } else {
      return this.renderEmpty();
    }
  }

  renderCart() {
    const cartItemsList = this.props.cart.map(item => {
      return (
        <Panel key={item._id}>
          <Row>
            <Col xs={12} sm={4}>
              <h6>{item.title}</h6>
            </Col>
            <Col xs={12} sm={2}>
              <h6>usd. {item.price}</h6>
            </Col>
            <Col xs={12} sm={2}>
              <h6>qty. <Label bsStyle="success">{item.quantity}</Label></h6>
            </Col>
            <Col xs={12} sm={4}>
              <ButtonGroup>
                <Button onClick={this.onDecrement.bind(this, item._id, item.quantity)} bsStyle="default" bsSize="small">-</Button>
                <Button onClick={this.onIncrement.bind(this, item._id)} bsStyle="default" bsSize="small">+</Button>
                <span>    </span>
                <Button onClick={this.onDelete.bind(this, item._id)} onClick={this.onDelete.bind(this, item._id)} bsStyle="danger" bsSize="small">DELETE</Button>
              </ButtonGroup>
            </Col>
          </Row>
        </Panel>
      );
    }, this);

    return (
      <Panel header="Cart" bsStyle="primary">
        {cartItemsList}
        <Row>
          <Col xs={12}>
            <h6>Total amount:</h6>
            <Button onClick={this.open.bind(this)} bsStyle="success" bsSize="small">
              PROCEED TO CHECKOUT
            </Button>
          </Col>
        </Row>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Thank you!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h6>Your order have been saved.</h6>
            <p>You will receive an email confirmation</p>
          </Modal.Body>
          <Modal.Footer>
            <Col sm={6}>
              <h6>Total $: {this.props.totalAmount}</h6>
            </Col>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </Panel>
    )
  }

  renderEmpty() {
    return(<div></div>);
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart.cart,
    totalAmount: state.cart.totalAmount
  }
}

export default connect(mapStateToProps, { deleteCartItem, updateCart, getCart })(Cart);
