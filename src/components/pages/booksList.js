import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Carousel, Grid, Row, Button, Col } from 'react-bootstrap';


import BooksForm from './booksForm';
import BookItem from './bookItem';
import Cart from './cart';
import { getBooks } from '../../actions/booksActions';

class BookList extends React.Component {
  componentDidMount() {
    this.props.getBooks();
  }
  render() {
    const booksList = this.props.books.map(function(book) {
      return (
        <Col xs={12} sm={6} md={4} key={book._id}>
          <BookItem
            _id={book._id}
            title={book.title}
            description={book.description}
            images={book.images}
            price={book.price}
          />
        </Col>
      )
    })

    return(
      <Grid>
        <Row style={{marginTop: '15px'}}>
          {booksList}
        </Row>
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return {
    books: state.books.books
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getBooks
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
