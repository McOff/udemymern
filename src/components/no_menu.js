import React from 'react';
import { Nav, NavItem, Navbar, Badge } from 'react-bootstrap';
import { Link, IndexLink } from 'react-router';

class Menu extends React.Component {
  render() {
    return (
      <Navbar inverse fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <IndexLink to="/">React-Bootstrap</IndexLink>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <li><Link to="/about" activeClassName="active">About</Link></li>
            <li><Link to="/contacts" activeClassName="active">Contact Us</Link></li>
          </Nav>
          <Nav pullRight>
            <li><Link to="/admin" activeClassName="active">Admin</Link></li>
            <li><Link to="/cart" activeClassName="active">
              Your Cart
              { (this.props.cartItemsNumber) > 0 ?
                <Badge className="badge">{this.props.cartItemsNumber}</Badge> :
                ''
              }
            </Link></li>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Menu;
