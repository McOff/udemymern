import React from 'react';
// import Menu from './menu';
import Hero from './common/hero';
import Footer from './common/footer';

import { connect } from 'react-redux';

import { getCart } from '../actions/cartActions';

class Main extends React.Component {
  componentDidMount() {
    this.props.getCart();
  }

  render() {
    return (
      <div>
        {this.props.children}
        <Footer />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    totalQty: state.cart.totalQty
  }
}

export default connect(mapStateToProps, { getCart })(Main);
