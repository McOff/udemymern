"use strict"
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';


import reducers from './reducers';
import { addToCart } from './actions/cartActions';
import { postBooks, deleteBooks, updateBooks } from './actions/booksActions';

// Styles
import './style/main.scss';

const middleware = applyMiddleware(thunk, logger);

//WE WILL PASS INITIAL STATE FROM SERVER STORE
const initialState = window.INITIAL_STATE;
const store = createStore(reducers, initialState, middleware);

import routes from './routes.js';

const Routes = (
  <Provider store={store}>
    {routes}
  </Provider>
);

render(
  Routes, document.getElementById('app')
);
