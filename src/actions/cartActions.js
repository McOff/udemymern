"use strict"
import axios from 'axios';

export function getCart() {
  return function(dispatch) {
    axios.get('api/cart')
      .then(response => {
        dispatch({ type: 'GET_CART', payload: response.data});
      })
      .catch(err => {
        dispatch({ type: 'GET_CART_REJECTION', msg: 'error' })
      })
  }
}

export function addToCart(cart) {
  return function(dispatch) {
    axios.post('api/cart', cart)
      .then(response => {
        dispatch({ type: 'ADD_TO_CART', payload: response.data });
      })
      .catch(err => {
        dispatch({ type: 'ADD_TO_CART_REJECTED', msg: 'error when adding to the cart' });
      })
  }
}

export function updateCart(_id, unit, cart) {

  return function(dispatch) {

    let booksToUpdate = cart;
    let idxToUpdate = booksToUpdate.findIndex( book => book._id === _id);
    booksToUpdate[idxToUpdate].quantity = booksToUpdate[idxToUpdate].quantity + unit;

    axios.post('api/cart', booksToUpdate)
      .then(response => {
        dispatch({ type: 'UPDATE_CART', payload: response.data });
      })
      .catch(err => {
        dispatch({ type: 'UPDATE_CART_REJECTED', msg: 'error when adding to the cart' });
      })
  }
}


export function deleteCartItem(cart) {
  return function(dispatch) {
    axios.post('api/cart', cart)
      .then(response => {
        dispatch({ type: 'DELETE_CART_ITEM', payload: response.data });
      })
      .catch(err => {
        dispatch({ type: 'DELETE_CART_ITEM', msg: 'error when deleting to the cart' });
      })
  }
  
}
