"use sctrict"
import axios from 'axios';
// GET A book
export function getBooks(books) {
  return function(dispatch) {
    axios.get('/api/books')
      .then(response => {
        dispatch({ type: "GET_BOOK", payload: response.data })
      })
      .catch(err => {
        dispatch({ type: "GET_BOOK_REJECTED", payload: err})
      });
  }
}

// ADD A book
export function postBooks(books) {
  return function (dispatch) {
    axios.post("/api/books", books)
      .then(response => {
        dispatch({ type: "POST_BOOK", payload: response.data})
      })
      .catch(err => {
        dispatch({ type: "POST_BOOK_REJECTION", payload: "there was an error"})
      });
  }
}

// DELETE A book
export function deleteBooks(book) {
  return function(dispatch) {
    axios.delete(`/api/books/${book._id}`)
      .then(response => {
        dispatch({ type: "DELETE_BOOK", payload: book })
      })
      .catch(err => {
        dispatch({ type: "DELETE_BOOK_REJECTION", payload: err})
      });
  }
}

//UPDATE A book
export function updateBooks(books) {
  return {
    type: "UPDATE_BOOK",
    payload: books
  }
}

export function resetButton(books) {
  return {
    type: "RESET_BUTTON"
  }
}
