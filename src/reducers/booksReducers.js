"use strict"

// const initState = { books: [
//   {
//     _id: 1,
//     title: 'this is the book',
//     description: 'book description',
//     price: 44
//   },
//   {
//     _id: 2,
//     title: 'this is the second book',
//     description: 'second book description',
//     price: 55
//   }
// ]};

export function booksReducers(state={ books: [] }, action) {
  switch (action.type) {

    case "GET_BOOK":
      return { ...state, books: [...action.payload] };

    case "POST_BOOK":
      return {
        ...state,
        books: state.books.concat(action.payload),
        msg: 'Saved! Click to continue',
        style: 'success',
        validation: 'success'
      };

    case "POST_BOOK_REJECTED":
      return {
        ...state,
        msg: 'Please, try again',
        style: 'danger',
        validation: 'error'
      };

    case "RESET_BUTTON":
      return {
        ...state,
        msg: '',
        style: 'primary',
        validation: null
      };

    case "DELETE_BOOK":
      let booksToDelete = [...state.books];
      let idxToDel = booksToDelete.findIndex( book => book._id.toString() === action.payload._id);
      return { books: [...booksToDelete.slice(0, idxToDel), ...booksToDelete.slice(idxToDel + 1)] };

    case "UPDATE_BOOK":
      let booksToUpdate = [...state.books];
      let idxToUpdate = booksToUpdate.findIndex( book => book._id === action.payload._id);
      booksToUpdate[idxToUpdate].title = action.payload.title;
      return { books: booksToUpdate };

    default:
      return state;
  }
}
